<?php
/**
 * @file
 *   Core Logger class for "Test Logging that Doesn't Suck (TL;DS)".
 *
 *   © 2017 Red Bottle Design, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\tlds\Logging;

/**
 * A lightweight object for logging messages, objects, and SQL queries to a
 * file.
 *
 * Object output is rendered by the `dump_r` PHP library by Leon Sorokin.
 */
class Logger {
  const DEFAULT_LOG_FILE = '/tmp/drupal_query_debug.log';

  private static $instance;

  protected $enabled;
  protected $logFilePath;
  protected $timestampsEnabled;

  /**
   * Gets or creates an instance of the TLDS logger for the current request.
   *
   * @return Logger
   *   The logger instance.
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new Logger();
    }

    return self::$instance;
  }

  /**
   * Constructor for Logger.
   *
   * The logger initially starts out disabled, with a default log path of
   * '/tmp/drupal_query_debug.log', and timestamping enabled.
   */
  public function __construct() {
    $this->setEnabled(FALSE);
    $this->setLogFilePath(self::DEFAULT_LOG_FILE);
    $this->setTimestampsEnabled(TRUE);
  }

  /**
   * Gets whether or not output from this logger is enabled.
   *
   * When output is not enabled, then everything written to the logger is
   * discarded.
   *
   * @return boolean
   */
  public function isEnabled() {
    return $this->enabled;
  }

  /**
   * Sets whether or not output from this logger is enabled.
   *
   * When output is not enabled, then everything written to the logger is
   * discarded.
   *
   * @param boolean $enabled
   *   TRUE to enable log output; or, FALSE, otherwise.
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * Gets whether or not per-entry time-stamping is enabled.
   *
   * @return boolean
   */
  public function areTimestampsEnabled() {
    return $this->timestampsEnabled;
  }

  /**
   * Sets whether or not per-entry time-stamping is enabled.
   *
   * Timestamps are always relative to GMT.
   *
   * @param boolean $timestampsEnabled
   *   Whether or not to prefix every log entry with a GMT timestamp.
   */
  public function setTimestampsEnabled($timestampsEnabled) {
    if (!is_bool($timestampsEnabled)) {
      throw new \InvalidArgumentException(
        '$timestampsEnabled must be either TRUE or FALSE (given '.
        "`$timestampsEnabled`).");
    }

    $this->timestampsEnabled = $timestampsEnabled;
  }

  /**
   * Gets the path being used for logging.
   *
   * @return string
   *   The path currently being used for logging.
   */
  public function getLogFilePath() {
    return $this->logFilePath;
  }

  /**
   * Sets the log file path.
   *
   * This will take effect the next time something is logged.
   *
   * @param string $logFilePath
   *   The path to the log file to use.
   */
  public function setLogFilePath($logFilePath) {
    if (empty($logFilePath)) {
      throw new \InvalidArgumentException(
        '$logFilePath cannot be NULL or empty.');
    }

    $this->logFilePath = $logFilePath;
  }

  /**
   * Writes a string of text to the query log.
   *
   * @param string $string
   *   The value to write.
   * @param string $label
   *   An optional label to prepend to the data.
   * @param bool $append
   *   Whether or not to append the new string to the existing log, or to
   *   truncate the log and then write the string to it.
   */
  public function writeLogEntry($string, $label = NULL, $append = TRUE) {
    if (!$this->isEnabled()) {
      return;
    }

    if ($this->areTimestampsEnabled()) {
      $timestamp = gmdate('D, d M Y H:i:s') . ' GMT';
      $prefix    = $timestamp . ': ';
    }
    else {
      $prefix = '';
    }

    if (!empty($label)) {
      $output = sprintf("%s%s: %s\n", $prefix, $label, $string);
    }
    else {
      $output = sprintf("%s%s\n", $prefix, $string);
    }

    if ($append) {
      $flags = FILE_APPEND;
    }
    else {
      $flags = 0;
    }

    file_put_contents($this->getLogFilePath(), $output, $flags);
  }

  /**
   * Writes an object or array out to the log.
   *
   * The log entry will contain an appropriate, human-friendly representation of
   * the object or array.
   *
   * @param mixed $object
   *   The primitive data type, object, or array to log out.
   * @param string $label
   *   An optional label to prepend to the data.
   * @param int $depth
   *   The recursion limit for the dump (defaults to 1,000 levels deep).
   * @param int $expand
   *   The auto-expanded child node depth (defaults to 1,000 levels deep).
   */
  public function writeObject($object, $label = NULL, $depth = 1000,
                              $expand = 1000) {
    if (!$this->isEnabled()) {
      return;
    }

    if (function_exists('dump_r')) {
      // dump_r() adds a carriage return to its output for us.
      $raw_dump = dump_r($object, TRUE, FALSE, $depth, $expand);
      $obj_dump = $this->replaceObjectDumpCallerInfo($raw_dump);
    }
    else {
      // Fallback in case dump_r has not yet loaded.
      // print_r() does not add a carriage return for us.
      $obj_dump = print_r($object, TRUE) . "\n";
    }

    // Adds an extra line before the object output
    $this->writeLogEntry("\n" . $obj_dump, $label);
  }

  /**
   * Writes a SQL query object out as a log entry.
   *
   * All indexed or named placeholders in the query are expanded.
   *
   * @param string|\DatabaseStatementInterface $query
   *   The query to expand and print.
   * @param array $args
   *   The placeholder arguments, keyed either positionally (`1`, `2`, `3`,
   *   etc.) or by name (`placeholder_1`, `placeholder_2`, etc.).
   * @param string $label
   *   An optional label to prepend to the query in the log.
   */
  public function writeQuery($query, array $args = array(), $label = NULL) {
    if (!$this->isEnabled()) {
      return;
    }

    $fullQuery = $this->expandPlaceholders($query->getQueryString(), $args);

    // Adds extra lines before and after the query output
    $this->writeLogEntry("\n" . $fullQuery . "\n", $label);
  }

  /**
   * Replaces all placeholders in a SQL query and returns the resulting string.
   *
   * Adapted from:
   * http://php.net/manual/en/pdostatement.debugdumpparams.php#113400
   *
   * @param string $query
   *   The SQL query that contains placeholder references.
   * @param array $placeholders
   *   An associative array, where each key is either a named or positional
   *   query placeholder and the value is the value to insert into the
   *   corresponding place in the query.
   *
   * @return string
   *   The full SQL query, with all matching placeholders replaced with their
   *   corresponding values.
   */
  protected function expandPlaceholders($query, $placeholders) {
    $indexed = ($placeholders == array_values($placeholders));

    foreach ($placeholders as $key => $replacement) {
      $escapedReplacement = $this->preparePlaceholderValue($replacement);

      if ($indexed) {
        $query = preg_replace('/\?/', $escapedReplacement, $query, 1);
      }
      else {
        // NOTE: Each key in the placeholder array already has ":" prepended
        $query = str_replace("$key", $escapedReplacement, $query);
      }
    }

    return $query;
  }

  /**
   * Converts placeholder replacements appropriately for a query.
   *
   * If the value is a string, all characters that are unsafe for MySQL string
   * are escaped.
   *
   * If the value is an array, all values are escaped and then joined together
   * in a comma-separated string.
   *
   * @param string|string[] $value
   *   Either a single string or an array of strings.
   *
   * @return string
   *   A string that represents the provided values, escaped so that the string
   *   can appear properly in the SQL query.
   */
  protected function preparePlaceholderValue($value) {
    if (is_array($value)) {
      $values = array();

      foreach ($value as $key => $currentValue) {
        $values[] = $this->preparePlaceholderValue($currentValue);
      }

      $result = join(',', $values);
    }
    else {
      $specialChars = array(
        '\\'    => '\\\\',
        "\0"    => '\\0',
        "\n"    => '\\n',
        "\r"    => '\\r',
        "'"     => "\\'",
        '"'     => '\\"',
        "\x1a"  => '\\Z',
      );

      $result = sprintf("'%s'", strtr($value, $specialChars));
    }

    return $result;
  }

  /**
   * Fixes the name of the calling file, and variable name from caller.
   *
   * Workaround for https://github.com/leeoniya/dump_r.php/issues/51
   *
   * @param $dumpROutput
   * @return string
   */
  protected function replaceObjectDumpCallerInfo($dumpROutput) {
    $callerInfo   = $this->getObjectDumpCallerInfo();
    $rawDumpLines = explode("\n", $dumpROutput);

    // Fix-up the caller's filename
    $rawDumpLines[0] =
      sprintf("%s (line %d)", $callerInfo['file'], $callerInfo['line']);

    // Fix-up the name of the provided variable from the caller
    $rawDumpLines[2] =
      preg_replace(
        '/^\$object/',
        $callerInfo['variable_provided'],
        $rawDumpLines[2], 1);

    $fixedOutput = implode("\n", $rawDumpLines);

    return $fixedOutput;
  }

  /**
   * Examines the stack to find out information on who called writeObject().
   *
   * The source file name, line number of the call, and the name of the variable
   * being passed-in to the call are identified and returned.
   *
   * @return array
   *   The caller info array.
   */
  protected function getObjectDumpCallerInfo() {
    $sourceFile         = '(unknown)';
    $sourceLineNumber   = '(unknown)';
    $sourceVariableName = '(unknown)';

    $stack       = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);
    $frameOffset = $this->findFirstNonTldsStackFrame($stack);

    if ($frameOffset != -1) {
      $stackFrame = $stack[$frameOffset];
    }

    if (isset($stackFrame['file']) && isset($stackFrame['line'])) {
      $sourceFile       = $stackFrame['file'];
      $sourceLineNumber = $stackFrame['line'];

      $callingMethodVariable =
        $this->getCallingMethodVariable($sourceFile, $sourceLineNumber);

      if (!empty($callingMethodVariable)) {
        $sourceVariableName = $callingMethodVariable;
      }
    }

    $callerInfo = array(
      'file'              => $sourceFile,
      'line'              => $sourceLineNumber,
      'variable_provided' => $sourceVariableName,
    );

		return $callerInfo;
  }

  /**
   * Gets the name of the variable that was passed into TLDS.
   *
   * This should be the name of the first argument passed to `writeObject` or
   * `tlds_log`.
   *
   * For performance reasons, this method will only search up to 10 lines of
   * source code to attempt to find the parameter. If it cannot be located or
   * recognized, NULL is returned.
   *
   * @param string $sourceFile
   *   The name of the source file from which the method was called.
   * @param int $sourceLineNumber
   *   The line number from which the method was called.
   *
   * @return string|NULL
   *   Either the name of the first variable passed to one of the recognized
   *   methods; or NULL, if it could not be found.
   */
  protected function getCallingMethodVariable($sourceFile, $sourceLineNumber) {
    $sourceVariableName = NULL;

    if (is_file($sourceFile)) {
      $allSourceLines    = file($sourceFile);
      $sourceLineMatches = FALSE;

      // Line numbers start at 1, so we have to switch to zero-based
      $scanLineMax = ($sourceLineNumber - 1);

      // Search only up to 10 lines back.
      $scanLineMin = max(0, $scanLineMax - 10);

      for ($scanLineNumber = $scanLineMax;
           ($scanLineNumber >= $scanLineMin) && !$sourceLineMatches;
           --$scanLineNumber) {
        $sourceLine = $allSourceLines[$scanLineNumber];

        $sourceLineMatches =
          preg_match(
            '/(?:(?:writeObject)|(?:tlds_log))\((.+?)\)?(?:$|;|\?>)/',
            $sourceLine,
            $matches);
      }

      if (!empty($matches)) {
        $sourceLineParamString = $matches[1];
        $sourceLineParams = explode(',', $sourceLineParamString);
        $sourceVariableName = trim($sourceLineParams[0]);
      }
    }

    return $sourceVariableName;
  }

  /**
   * Gets the first stack frame that does not lie within internal TLDS code.
   *
   * @param array $stack
   *   A stack backtrace, as returned by `debug_backtrace()`.
   *
   * @return int|NULL
   *   Either the offset of the first non-TLDS stack frame; or, NULL, if it
   *   could not be located.
   */
  protected function findFirstNonTldsStackFrame($stack) {
    $result = -1;

    $frameCount = count($stack);

    for ($frameIndex = 0; $frameIndex < $frameCount; ++$frameIndex) {
      $currentFrame = $stack[$frameIndex];

      $isTldsClass =
        isset($currentFrame['class']) &&
        ($currentFrame['class'] == 'Drupal\tlds\Logging\Logger');

      $isTldsFunction =
        isset($currentFrame['file']) &&
        basename($currentFrame['file']) == 'tlds.module';

      // FIXME: Find a better way to abstract the special case for TLDS'
      //        hook_exit() call to writeObject().
      if (isset($currentFrame['function']) &&
              ($currentFrame['function'] == 'tlds_exit')) {
        $result = $frameIndex - 1;
        break;
      }

      if (!$isTldsClass && !$isTldsFunction) {
        $result = $frameIndex;
        break;
      }
    }

    return $result;
  }
}
