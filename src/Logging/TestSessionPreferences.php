<?php
/**
 * @file
 *   Preference save & store logic for "Test Logging that Doesn't Suck (TL;DS)".
 *
 *   © 2017 Red Bottle Design, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\tlds\Logging;

/**
 * Saves and restores preferences from a Drupal variable.
 *
 * This is used to keep track of the settings for current test session, in
 * between requests.
 */
class TestSessionPreferences {
  //============================================================================
  // Constants
  //============================================================================
  const VAR_PREFS                 = '_tlds_session_preferences';
  const PREF_OUTPUT_ENABLED       = 'output_enabled';
  const PREF_OUTPUT_PATH          = 'log_path';
  const PREF_TIMESTAMPS_ENABLED   = 'timestamps_enabled';
  const PREF_CONNECTIONS_LOGGING  = 'db_connections_with_logging';

  //============================================================================
  // Public Instance Methods
  //============================================================================
  /**
   * Saves preferences for the specified logger to a Drupal variable.
   *
   * @param Logger $logger
   *   The logger for which to save preferences.
   */
  public function saveFrom(Logger $logger) {
    $preferences = $this->getCurrent($logger);

    variable_set(self::VAR_PREFS, $preferences);
  }

  /**
   * Restores preferences to the specified logger from a Drupal variable.
   *
   * @param Logger $logger
   *   The logger to which preferences are to be restored.
   */
  public function restoreTo(Logger $logger) {
    $preferences = variable_get(self::VAR_PREFS, NULL);

    if ($preferences !== NULL) {
      $logger->setEnabled($preferences[self::PREF_OUTPUT_ENABLED]);
      $logger->setLogFilePath($preferences[self::PREF_OUTPUT_PATH]);
      $logger->setTimestampsEnabled(
        $preferences[self::PREF_TIMESTAMPS_ENABLED]);

      $connectionsWithLogging = $preferences[self::PREF_CONNECTIONS_LOGGING];

      foreach ($connectionsWithLogging as $key => $targets) {
        foreach ($targets as $target) {
          $connection = \Database::getConnection($target, $key);

          $connection->setLogger(
            new DatabaseLogWrapper($logger, $target, $key));
        }
      }
    }
  }

  /**
   * Cleans up the variable that preferences are stored in.
   */
  public function cleanup() {
    variable_del(self::VAR_PREFS);
  }

  /**
   * Extracts preferences from the specified logger into an array.
   *
   * @param Logger $logger
   *   The logger from which to extract preferences.
   *
   * @return array
   *   The array of test session preferences based on the logger's current
   *   configuration.
   */
  public function getCurrent(Logger $logger) {
    $preferences = array(
      self::PREF_OUTPUT_ENABLED       => $logger->isEnabled(),
      self::PREF_OUTPUT_PATH          => $logger->getLogFilePath(),
      self::PREF_TIMESTAMPS_ENABLED   => $logger->areTimestampsEnabled(),
      self::PREF_CONNECTIONS_LOGGING  =>
        DatabaseLogWrapper::getDatabaseConnectionsWithLogging(),
    );

    return $preferences;
  }
}
