<?php
/**
 * @file
 *   Database Log Wrapper for "Test Logging that Doesn't Suck (TL;DS)".
 *
 *   © 2017 Red Bottle Design, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
namespace Drupal\tlds\Logging;

/**
 * A Drupal DatabaseLogWrapper that logs its output to TLDS.
 *
 * For memory usage and performance reasons, this class does not maintain the
 * internal copy of queries that is normally provided by the DatabaseLog class.
 * Consequently, all methods other than log() are no-ops.
 *
 * @package Drupal\tlds\Logging
 */
class DatabaseLogWrapper extends \DatabaseLog {
  //============================================================================
  // Static Fields
  //============================================================================
  private static $databaseConnections;

  //============================================================================
  // Instance Fields
  //============================================================================
  protected $tldsLogger;

  //============================================================================
  // Constructor
  //============================================================================
  /**
   * Constructor for DatabaseLogWrapper.
   *
   * @param Logger $tldsLogger
   *   The TLDS logger that this is wrapping.
   * @param string $target
   *   The database target name for which to enable logging.
   * @param string $key
   *   The database connection key for which to enable logging.
   */
  public function __construct(Logger $tldsLogger, $target = 'default',
                              $key = NULL) {
    parent::__construct($key);

    $this->setTldsLogger($tldsLogger);

    self::monitorConnection($key, $target);
  }

  //============================================================================
  // Static Methods
  //============================================================================
  /**
   * Gets all database connections that this class is monitoring.
   *
   * This may include connections for which logging was initially enabled and
   * then disabled afterwards.
   *
   * @see getDatabaseConnectionsWithLogging
   *
   * @return array
   *   An associative array where each key is a database connection key, and
   *   the value is an array of target names.
   */
  public static function getDatabaseConnections() {
    if (empty(self::$databaseConnections)) {
      self::$databaseConnections = array();
    }

    return self::$databaseConnections;
  }

  /**
   * Gets all database connections this class knows has logging enabled.
   *
   * @return array
   *   An associative array where each key is a database connection key, and
   *   the value is an array of target names.
   */
  public static function getDatabaseConnectionsWithLogging() {
    $connectionsWithLogging = array();
    $allConnections         = self::getDatabaseConnections();

    foreach ($allConnections as $key => $targets) {
      foreach ($targets as $target) {
        $connection = \Database::getConnection($target, $key);
        $logger     = $connection->getLogger();

        if ($logger instanceof DatabaseLogWrapper) {
          if (!isset($connectionsWithLogging[$key])) {
            $connectionsWithLogging[$key] = array();
          }

          $connectionsWithLogging[$key][] = $target;
        }
      }
    }

    return $connectionsWithLogging;
  }

  /**
   * Sets the database connections that this object is monitoring.
   *
   * @param array $databaseConnections
   *   An associative array where each key is a database connection key, and
   *   the value is an array of target names.
   */
  protected static function setDatabaseConnections($databaseConnections) {
    self::$databaseConnections = $databaseConnections;
  }

  /**
   * Adds a database connection to the list of connections this class monitors.
   *
   * @param $target
   *   The database target name.
   * @param $key
   *   The database connection key.
   */
  protected static function monitorConnection($target, $key) {
    $connections = self::getDatabaseConnections();

    // Special case: $key can be NULL to use the database's active key.
    // We handle $target here the same way, just in case.
    if (empty($target) || empty($key)) {
      $connection = \Database::getConnection($target, $key);
      $key        = $connection->getKey();
      $target     = $connection->getTarget();
    }

    if (!isset($connections[$key])) {
      $connections[$key] = array();
    }

    if (!in_array($target, $connections[$key])) {
      $connections[$key][] = $target;
    }

    self::setDatabaseConnections($connections);
  }

  //============================================================================
  // Accessors and Mutators
  //============================================================================
  /**
   * Gets the TLDS logger this instance is wrapping.
   *
   * @return Logger
   */
  public function getTldsLogger() {
    return $this->tldsLogger;
  }

  /**
   * Sets the TLDS logger this instance is wrapping.
   *
   * @param Logger $tldsLogger
   *   The new instance to wrap.
   */
  public function setTldsLogger(Logger $tldsLogger) {
    $this->tldsLogger = $tldsLogger;
  }

  //============================================================================
  // Public Instance Methods
  //============================================================================
  public function log(\DatabaseStatementInterface $statement, $args, $time) {
    $logger = $this->getTldsLogger();

    $logger->writeQuery($statement, $args, "Query executed ($time msec)");
  }

  public function start($logging_key) {
    // Intentional no-op
  }

  public function get($logging_key) {
    // Intentional no-op
    return array();
  }

  public function clear($logging_key) {
    // Intentional no-op
  }

  public function end($logging_key) {
    // Intentional no-op
  }
}
