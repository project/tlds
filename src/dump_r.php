<?php
/**
 * @file
 *   Custom bootstrap logic for dump_r().
 *
 *   © 2017 Red Bottle Design, LLC. All rights reserved.
 *
 * @author Guy Elsmore-Paddock (guy@redbottledesign.com)
 */
define('TLDS_DUMPR_PATH', __DIR__ . '/../vendor/dump_r/');

require(TLDS_DUMPR_PATH . 'lib/PSR4_Loader.php');

$loader = new Psr4AutoloaderClass;
$loader->register();
$loader->addNamespace('dump_r\\', TLDS_DUMPR_PATH . '/src/dump_r');

require(TLDS_DUMPR_PATH . 'dump_r.php');
