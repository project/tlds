# Test Logging that Doesn't Suck (TL;DS)
This module is an attempt to provide a way to log debug information and all SQL
queries during SimpleTest test cases, and from within module code, in a better
way than any other approach currently available.

## Credits
- Guy Elsmore-Paddock (Module Author)
- Leon Sorokin (Author of `dump_r`)

## Features
This module provides:
- A straightforward logging interface to use before, during, and after
  SimpleTest test cases via the new `tlds_log()` method. It's a drop-in
  replacement for `kpr()`, `dpm()`, or `print_r()`.
- Optionally-timestamped log lines.
- Log output that can contain:
  - All SQL queries executed by the database, with parameter substitutions
    automatically filled-in for you.
  - Plain text debug messages.
  - SQL query objects you pass-in manually (again, with automatic fill-in of
    parameter substitutions).
  - Human-friendly and objects, generated using
    [Leon Sorokin](https://github.com/leeoniya)'s excellent
    [`dump_r()`](https://github.com/leeoniya/dump_r.php) replacement for
    `print_r()` and `var_dump()`. Even handles resources, closures, and
    recursive references!

## Using TL;DS in Tests
The interface for TL;DS is very straightforward.

The most common function you'll use is `tlds_log()`, which works very similarly
to `kpr()` or `dpm()` from Devel / Krumo. There are also some convenience
methods provided in `tlds.module` for common tasks, but many are just wrappers
around the actual methods in the `\Drupal\tlds\Logging\Logger` class.

You can get an instance of the current `Logger` by calling
`Logger::getInstance()` or by using `tlds_get_logger()` (which wraps the former
method).

TL;DS consists of three components:
- The logging interface itself, which you use to write out messages and values
  to the log.
- The Database log wrapper, which you use to capture SQL queries that are
  happening on the site or during a test run so that they are written out to
  the log.
- Test session persistence, which is needed to save the state of the logger
  between the various requests that SimpleTest executes throughout the test.
  
### Setup
In order for your log output to go anywhere, you need to configure where log
output should be written and enable the logger.

This is commonly done from within your test setup, as follows:
```PHP
<?php
abstract class MyTestCase extends DrupalWebTestCase {
  public function setUp() {
    parent::setUp(array('tlds'));
    
    $this->setupAndStartLogger();
  }

  public function tearDown() {
    parent::tearDown();

    $this->stopLogger();
  }

  protected function setupAndStartLogger() {
    $testId = $this->databasePrefix;

    $logPath = $this->getLogPath();
    $logDir  = dirname($logPath);

    if (!is_dir($logDir)) {
      mkdir($logDir, 0777, TRUE);
    }

    tlds_set_log_file_path($logPath);
    tlds_start_new_test_log($testId);
    tlds_enable_query_logging();
    tlds_save_test_session_preferences();
  }

  protected function stopLogger() {
    $testId = $this->databasePrefix;

    tlds_end_test_log($testId);
  }
  
  protected function getLogPath() {
    $testId  = $this->databasePrefix;
    $logPath =
      sprintf(
        '/tmp/simpletest_logs/MyTestCase/%s_%s.log',
        get_class($this),
        $testId);

    return $logPath;
  }
}
```

This type of setup would give you the flexibility to override where output is
written for each test case by overriding the `getLogPath()` method. In addition,
by using the database prefix in the name of the log, you are guaranteeing that
each individual test within the test case produces its own log file.

As shown in the previous example, you need to make sure to call
`tlds_save_test_session_preferences()` during the test setup after you are done
configuring the logger, or you will not get any log output during the tests.
This method uses TL;DS's preferences persistence module to save the state of
the logger so that it can be referenced during each test request.

If the log file path is not set, it defaults to `/tmp/drupal_query_debug.log`.

### Writing to the Log
You can use `tlds_log()` at any point during test setup, in module code, during
tests, or during test teardown. It works the same way as `dpm()` and `kpr()`.

For example:
```PHP
<?php
  function my_func($values) {
    tlds_log('Entering `my_func`');
  
    // Second parameter is an optional label, just like with dpm()
    tlds_log($values, 'The source values');
  }

  $my_array = array(1 => 'A', 2 => 'B', 3 => 'C');

  my_func($my_array);
```

If test output is enabled at the time you call `tlds_log()`, then it is written
out to the log. If not, your message is discarded.
